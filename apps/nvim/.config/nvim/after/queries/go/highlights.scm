; extends

((selector_expression) @method (#eq? @method "fx.Provide") (#set! conceal "🎁"))
((selector_expression) @method (#eq? @method "fx.Invoke") (#set! conceal "👇"))
((selector_expression) @method (#eq? @method "fmt.Errorf") (#set! conceal "👇"))
