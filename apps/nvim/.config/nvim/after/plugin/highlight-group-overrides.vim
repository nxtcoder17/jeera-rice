hi! DiffAdd guifg = #81A1C1 guibg = none
hi! GitSignsAdd guifg = #81A1C1 guibg = none
hi! DiffChange guifg = #9a7ecc guibg = none
hi! GitSignsChange guifg = #4e5173  guibg = none
hi! DiffModified guifg = #C88463 guibg = none
hi! GitSignsModified guifg = #C88463 guibg = none
hi! DiffDelete guifg = #D76363 guibg = none
hi! GitSignsDelete guifg = #D76363 guibg = none

" hi! Statusline guifg=#81A1C1 guibg=none gui=italic
" hi! StatuslineNC guifg=#81A1C1 gui=bold
hi! Statusline guibg=none
hi! StatuslineNC guibg=none

" hi! TabLineSel guifg=#2b3038 guibg=#81A1C1
" hi! TabLine guifg=#81A1C1 guibg=#272e3b gui=italic

hi! link MsgArea DiffAdd

" for mini statusline
hi! link MiniStatuslineModeNormal DiffAdd
" hi! MiniStatuslineFilename guifg=#81A1C1 guibg=transparent gui=bold
hi! MiniStatuslineFilename guibg=transparent gui=bold
" hi! MiniStatuslineFileinfo  guifg=#81A1C1 guibg=#272e3b gui=bold
" hi! MiniStatuslineDevinfo  guifg=#81A1C1 guibg=#273b36 gui=italic

