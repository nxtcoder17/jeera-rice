------ Nvim Core KeyMappings ------

-- resets
vim.keymap.set({ "n", "v" }, ";", ":")
-- vim.keymap.set({ "n", "v" }, "f", "<Nop>")

vim.keymap.set("n", "j", "gj")
vim.keymap.set("n", "k", "gk")

vim.keymap.set("t", "<esc>", "<C-\\><C-N>")
vim.keymap.set({ "n", "v" }, "cc", '"+y')

vim.keymap.set({ "n" }, "<leader>f", "<Cmd>Telescope current_buffer_fuzzy_find<CR>")

vim.g.mapleader = ","

-- [ the 's' key ]
vim.keymap.set({ "n", "v" }, "s", "<Nop>", { silent = true, noremap = true })
vim.keymap.set({ "n", "v" }, "ss", ":w<CR>")

vim.keymap.set({ "n" }, "st", "<Cmd>ToggleTerm direction=float<CR>")

-- making splits
vim.keymap.set("n", "si", ":vsplit<CR>")
vim.keymap.set("n", "sm", ":split<CR>")

-- split resize
vim.keymap.set({ "n" }, "<C-M-Left>", "<Cmd>vertical resize -5<CR>")
vim.keymap.set({ "n" }, "<C-M-Right>", "<Cmd>vertical resize +5<CR>")
vim.keymap.set({ "n" }, "<C-M-Up>", "<Cmd>resize -5<CR>")
vim.keymap.set({ "n" }, "<C-M-Down>", "<Cmd>resize +5<CR>")

-- better copy pasting
vim.keymap.set("n", "sp", '"_dP')

vim.keymap.set("n", "sb", require("telescope.builtin").buffers, { silent = true, noremap = true })

-- -- clean other buffers
-- vim.keymap.set("n", "x", function() require("mini.bufremove").wipeout(buf_id, force) end)

-- split navigation
vim.keymap.set("n", "sh", "<C-w>h<CR>")
vim.keymap.set("n", "sl", "<C-w>l<CR>")
vim.keymap.set("n", "sj", "<C-w>j<CR>")
vim.keymap.set("n", "sk", "<C-w>k<CR>")

-- buffers closing others
vim.keymap.set("n", "sx", ":BufDelOthers")

-- tabs
vim.cmd("cnoreabbrev tcd silent! windo tcd")
vim.keymap.set("n", "tn", "<cmd>tabnew<CR>|:windo tcd " .. vim.g.root_dir .. "<CR>", { silent = true })
vim.keymap.set("n", "te", "<cmd>tabedit % |:windo tcd " .. vim.g.root_dir .. "<CR>", { silent = true })

vim.keymap.set("n", "<BS>", ":set nohls <CR>|:lua Fn().closeFloating() <CR>")

-- creating scratch files
vim.api.nvim_create_user_command("Scratch", function()
  vim.cmd("vne | setlocal buftype=nofile | setlocal bufhidden=hide | setlocal noswapfile")
end, {})

-- because rnvimr shits wqa
vim.keymap.set("c", "wqa", "wa! | qa!")

-- [Non-core Keymappings]

-- telescope
-- vim.keymap.set("n", "sf", ":Telescope find_files<CR>")
vim.keymap.set("n", "sf", require("nxtcoder17.plugins.telescope").live_files)
vim.keymap.set("n", "ff", require("nxtcoder17.plugins.telescope").grep)
vim.keymap.set("n", "tl", require("nxtcoder17.plugins.telescope").only_tabs, { silent = true, noremap = true })

vim.keymap.set("n", "<M-o>", ":RnvimrToggle<CR>")
vim.keymap.set("t", "<M-o>", "<C-\\><C-n>:RnvimrToggle<CR>")

vim.keymap.set("n", "<C-;>", require("nxtcoder17.plugins.telescope").dapActions)

-- vim.keymap.set("n", "fd", require("nxtcoder17.plugins.telescope").dapActions)
-- vim.keymap.set("n", "f'", require("nxtcoder17.plugins.telescope").actions)

-- vim.keymap.set("n", "<M-k>", function()
--   require("nxtcoder17.functions.treesitter-queries").jumps(nil, nil, { up = true })
-- end)

-- vim.keymap.set("n", "<M-j>", function()
--   require("nxtcoder17.functions.treesitter-queries").jumps(nil, nil, { down = true })
-- end)

vim.keymap.set("n", "<M-Left>", require("nvim-tmux-navigation").NvimTmuxNavigateLeft)
vim.keymap.set("n", "<M-Right>", require("nvim-tmux-navigation").NvimTmuxNavigateRight)
vim.keymap.set("n", "<M-Down>", require("nvim-tmux-navigation").NvimTmuxNavigateDown)
vim.keymap.set("n", "<M-Up>", require("nvim-tmux-navigation").NvimTmuxNavigateUp)

-- [[ DAP ]]

-- luasnip
vim.cmd([[
  imap <silent><expr> <C-n> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-n>'
  smap <silent><expr> <C-n> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-n>'
  imap <silent><expr> <C-p> luasnip#choice_active() ? '<Plug>luasnip-prev-choice' : '<C-p>'
  smap <silent><expr> <C-p> luasnip#choice_active() ? '<Plug>luasnip-prev-choice' : '<C-p>'
]])

vim.keymap.set({ "i", "s" }, "<C-l>", function()
  if require("luasnip").choice_active() then
    require("luasnip").change_choice(1)
  end
end)

vim.keymap.set({ "i", "s" }, "<C-h>", function()
  if require("luasnip").choice_active() then
    require("luasnip").change_choice(-1)
  end
end)

vim.keymap.set("n", "<leader>sr", function()
  require("spectre").open_visual({ select_word = true })
end)

vim.keymap.set("n", "<leader>r", function()
  require("replacer").run()
end)
