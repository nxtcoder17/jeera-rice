vim.g.gruvbox_material_better_performance = 1
vim.g.gruvbox_material_background = "hard"
vim.g.gruvbox_material_foreground = "material"

-- vim.g.gruvbox_material_colors_override = {}

-- vim.cmd("colorscheme gruvbox-material")
-- vim.cmd("hi! Statement guifg=##54b8b1")
