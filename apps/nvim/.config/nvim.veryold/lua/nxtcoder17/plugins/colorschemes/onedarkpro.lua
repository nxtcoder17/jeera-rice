require("onedarkpro").setup({
  colors = {
    onedark_vivid = {
      -- red = "#FF0000",
      -- red = "#a86c42",
      red = "#aebd93",
      purple = "#65a1ba",
    },
  },
})

-- vim.cmd("colorscheme onedark_vivid")
