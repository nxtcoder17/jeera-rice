vim.g.substrata_italic_functions = true
vim.g.substrata_italic_comments = true
vim.g.substrata_italic_keywords = true
vim.g.substrata_italic_booleans = true
vim.g.substrata_transparent = true
vim.g.substrata_variant = "brighter"

vim.cmd("colorscheme substrata")
