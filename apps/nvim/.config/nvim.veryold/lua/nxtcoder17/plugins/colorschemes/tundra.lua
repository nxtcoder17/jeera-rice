require("nvim-tundra").setup({
	transparent_background = true,
})

vim.cmd("colorscheme tundra")
