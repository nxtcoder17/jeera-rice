vim.g.gruvbox_material_foreground = "mix"
vim.g.gruvbox_material_background = "hard"

vim.g.gruvbox_material_colors_override = {
  -- ["red"] = { "#3bd2f7", 167 },
  ["red"] = { "#a7bbeb", 167 },
  ["orange"] = { "#b39e86", 208 },
  -- ["yellow"] = { "#c2aa84", 214 },
  -- ["yellow"] = { "#c4b184", 214 },
  ["yellow"] = { "#bd9c42", 214 },
}

vim.cmd("colorscheme gruvbox-material")
