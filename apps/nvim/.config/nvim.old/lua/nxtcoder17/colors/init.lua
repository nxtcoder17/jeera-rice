local M = {}

M.palette = {
	["mantis"] = {
		["50"] = "#f6faf3",
		["100"] = "#e9f5e3",
		["200"] = "#d3eac8",
		["300"] = "#afd89d",
		["400"] = "#82bd69",
		["500"] = "#61a146",
		["600"] = "#4c8435",
		["700"] = "#3d692c",
		["800"] = "#345427",
		["900"] = "#2b4522",
		["950"] = "#13250e",
	},

	-- #ba7264
	["contessa"] = {
		["50"] = "#fbf6f5",
		["100"] = "#f6ecea",
		["200"] = "#f0dcd8",
		["300"] = "#e4c3bd",
		["400"] = "#d3a096",
		["500"] = "#ba7264",
		["600"] = "#aa6558",
		["700"] = "#8e5347",
		["800"] = "#77463d",
		["900"] = "#643f38",
		["950"] = "#351e1a",
	},

	-- #000000
	["black"] = {
		["50"] = "#f6f6f6",
		["100"] = "#e7e7e7",
		["200"] = "#d1d1d1",
		["300"] = "#b0b0b0",
		["400"] = "#888888",
		["500"] = "#6d6d6d",
		["600"] = "#5d5d5d",
		["700"] = "#4f4f4f",
		["800"] = "#454545",
		["900"] = "#3d3d3d",
		["950"] = "#000000",
	},

	["rose-of-sharon"] = {
		["50"] = "#fffbeb",
		["100"] = "#fef3c7",
		["200"] = "#fde58a",
		["300"] = "#fbd24e",
		["400"] = "#fabe25",
		["500"] = "#f49d0c",
		["600"] = "#d87607",
		["700"] = "#bc560a",
		["800"] = "#923f0e",
		["900"] = "#78340f",
		["950"] = "#451a03",
	},

	-- #438e96
	["blue-chill"] = {
		["50"] = "#f2f9f9",
		["100"] = "#ddeff0",
		["200"] = "#bfe0e2",
		["300"] = "#92cace",
		["400"] = "#5faab1",
		["500"] = "#438e96",
		["600"] = "#3b757f",
		["700"] = "#356169",
		["800"] = "#325158",
		["900"] = "#2d464c",
		["950"] = "#1a2c32",
	},

	-- #e6edd5
	["kidnapper"] = {
		["50"] = "#f5f8ed",
		["100"] = "#e6edd5",
		["200"] = "#d2e0b6",
		["300"] = "#b6cb8b",
		["400"] = "#9ab566",
		["500"] = "#7d9a48",
		["600"] = "#607a36",
		["700"] = "#4b5e2d",
		["800"] = "#3d4c28",
		["900"] = "#364225",
		["950"] = "#1b2310",
	},

	["bermuda-gray"] = {
		["50"] = "#f4f6f7",
		["100"] = "#e2e8eb",
		["200"] = "#c9d3d8",
		["300"] = "#a3b4bd",
		["400"] = "#728997",
		["500"] = "#5b717f",
		["600"] = "#4e5f6c",
		["700"] = "#44515a",
		["800"] = "#3d454d",
		["900"] = "#363d43",
		["950"] = "#21262b",
	},

	-- #607d8b on uicolors.app
	["smalt-blue"] = {
		["50"] = "#f4f6f7",
		["100"] = "#e2e8eb",
		["200"] = "#c8d4d9",
		["300"] = "#a2b6be",
		["400"] = "#74909c",
		["500"] = "#607d8b",
		["600"] = "#4d616d",
		["700"] = "#42515c",
		["800"] = "#3c474e",
		["900"] = "#353d44",
		["950"] = "#20272c",
	},

	-- #9e9e9e on uicolors.app
	["star-dust"] = {
		["50"] = "#f7f7f7",
		["100"] = "#ededed",
		["200"] = "#dfdfdf",
		["300"] = "#c8c8c8",
		["400"] = "#adadad",
		["500"] = "#9e9e9e",
		["600"] = "#888888",
		["700"] = "#7b7b7b",
		["800"] = "#676767",
		["900"] = "#545454",
		["950"] = "#363636",
	},

	-- #888888 on uicolors.app
	["gunsmoke"] = {
		["50"] = "#f6f6f6",
		["100"] = "#e7e7e7",
		["200"] = "#d1d1d1",
		["300"] = "#b0b0b0",
		["400"] = "#888888",
		["500"] = "#6d6d6d",
		["600"] = "#5d5d5d",
		["700"] = "#4f4f4f",
		["800"] = "#454545",
		["900"] = "#3d3d3d",
		["950"] = "#262626",
	},

	["bright-gray"] = {
		["50"] = "#f7f7f8",
		["100"] = "#eeedf1",
		["200"] = "#d8d8df",
		["300"] = "#b5b5c4",
		["400"] = "#8d8ea3",
		["500"] = "#6f7088",
		["600"] = "#595970",
		["700"] = "#49495b",
		["800"] = "#40404f",
		["900"] = "#373743",
		["950"] = "#26252c",
	},

	["curious-blue"] = {
		["50"] = "#f0faff",
		["100"] = "#e0f4fe",
		["200"] = "#b9eafe",
		["300"] = "#7cdbfd",
		["400"] = "#36cafa",
		["500"] = "#0cb4eb",
		["600"] = "#0097d1",
		["700"] = "#0173a3",
		["800"] = "#066186",
		["900"] = "#0b506f",
		["950"] = "#07334a",
	},

	["picton-blue"] = {
		["50"] = "#f0faff",
		["100"] = "#e0f4fe",
		["200"] = "#b9eafe",
		["300"] = "#7cdbfd",
		["400"] = "#36cbfa",
		["500"] = "#0cb4eb",
		["600"] = "#0092c9",
		["700"] = "#0273a2",
		["800"] = "#066186",
		["900"] = "#0b506f",
		["950"] = "#07334a",
	},

	["mandy"] = {
		["50"] = "#fef2f3",
		["100"] = "#fde6e7",
		["200"] = "#fbd0d5",
		["300"] = "#f7aab2",
		["400"] = "#f27a8a",
		["500"] = "#ea546c",
		["600"] = "#d5294d",
		["700"] = "#b31d3f",
		["800"] = "#961b3c",
		["900"] = "#811a39",
		["950"] = "#48091a",
	},

	-- vim.cmd([[let @a = "^[0ca'[^[pf:<80><fd>5r=<80><fd>5j"]])
	["purple-heart"] = {
		["50"] = "#f3f1ff",
		["100"] = "#ebe5ff",
		["200"] = "#d9ceff",
		["300"] = "#bea6ff",
		["400"] = "#9f75ff",
		["500"] = "#843dff",
		["600"] = "#7916ff",
		["700"] = "#6b04fd",
		["800"] = "#5a03d5",
		["900"] = "#4b05ad",
		["950"] = "#2c0076",
	},
}

-- source: https://www.reddit.com/r/GoodNotes/comments/s91wwi/zebra_sarasa_pen_color_hex_codes/
M.gel_pen_variants = {
	["black"] = "#252724",
	["blue-black"] = "#0D314B",
	["blue"] = "#162F57",
	["cobalt-blue"] = "#2F5BA2",
	["pale-blue"] = "#2493C0",
	["string"] = "#1f719b",
	["light-blue"] = "#6ACFEB",
	-- ["Deep Cerulean"] = "#1f617f",
	["blue-green"] = "#60D3DA",
	["vividian-green"] = "#047D54",
	["yellow"] = "#F4BC5B",
	["orange"] = "#ED7E39",
	["red-orange"] = "#E96B3B",
	["red"] = "#F6474E",
	["purple"] = "#E942CE",
	["magenta-pink"] = "#E92DA1",
	["pink"] = "#F15988",
	["light-pink"] = "#F581C8",
}

M.chocolate = "#7b3f00"

return M
